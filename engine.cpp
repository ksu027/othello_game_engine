#include "engine.h"
#include <future>
#include <chrono>

namespace othello
{

bool OthelloGameEngine::initNewGame()
{
  /*reset bitsets*/
  clearGame();

  /*set starting black pieces*/
  m_board[size_t(othello::PlayerId::One)].set(28);
  m_board[size_t(othello::PlayerId::One)].set(35);
  /*set starting white pieces*/
  m_board[size_t(othello::PlayerId::Two)].set(27);
  m_board[size_t(othello::PlayerId::Two)].set(36);
  board_before_turn=m_board;
  cur_player_id=othello::PlayerId::Two;

  return true;
}

void OthelloGameEngine::clearGame() {
    /*reset bitsets*/
    othello::utility::clearBoard(m_board);
    /*reset score*/
    othello::utility::clearBoard(board_before_turn);
    score_player_one=score_player_two=0;
}

bool OthelloGameEngine::performMoveForCurrentHuman(const BitPos& board_pos)
{
  // check if it is a legal position
  // call flip
  if (!othello::utility::isLegalMove(m_board,cur_player_id,board_pos)) {return false;};
  othello::utility::placeAndFlip(m_board,currentPlayerId(),board_pos);
  return true;
}

void OthelloGameEngine::changePlayer ()
{
    cur_player_id=othello::utility::opponent_player(cur_player_id);
    return;
};

bool OthelloGameEngine::endOfGame()
{
    if (othello::utility::legalMoves(m_board,cur_player_id).empty() and othello::utility::legalMoves(m_board,othello::utility::opponent_player(cur_player_id)).empty()) {
        return true;
    } else { return false;};
}

bool OthelloGameEngine::passTurn()
{
    if ((othello::utility::legalMoves(m_board,cur_player_id).empty()) and (!(othello::utility::legalMoves(m_board,othello::utility::opponent_player(cur_player_id)).empty()))) {
        return true;
    } else { return false;};
}

void OthelloGameEngine::think(const std::chrono::seconds& time_limit)
{
    //std::vector<std::future<bool>> futures;

    if (currentPlayerType()==othello::PlayerType::Human) {return;}
    else {
        if (currentPlayerId()==PlayerId::One) {
            m_player_one.obj->think(m_board,cur_player_id,time_limit);
            //futures.push_back( std::async( std::launch::async,std::mem_fn(&PlayerInterface::think),&m_player_one.obj->think(m_board,cur_player_id,time_limit)));
            performMoveForCurrentHuman(m_player_one.obj->bestMove());
        }
        else {
            m_player_two.obj->think(m_board,cur_player_id,time_limit);
            performMoveForCurrentHuman(m_player_two.obj->bestMove());
        }
    };



    return;

}

PlayerId OthelloGameEngine::currentPlayerId() const
{
  return cur_player_id;
}

PlayerType OthelloGameEngine::currentPlayerType() const
{
    if (currentPlayerId()==othello::PlayerId::One) {return m_player_one.type;}
    else {return m_player_two.type;}

}

BitPieces OthelloGameEngine::pieces(const PlayerId& player_id) const
{

    return m_board[size_t(PlayerId(player_id))];
}

void OthelloGameEngine::countScore(const othello::PlayerId& player_id)
{    
    if (player_id==othello::PlayerId::One) { if (m_board!=board_before_turn)
        score_player_one=score_player_one+int(pieces(player_id).count()-board_before_turn[size_t(player_id)].count()-1);
    } else { if (m_board!=board_before_turn) score_player_two=score_player_two+int(pieces(player_id).count()-board_before_turn[size_t(player_id)].count()-1);}

    board_before_turn=board();
}
const BitBoard& OthelloGameEngine::board() const { return m_board; }

}
// namespace othello
