#ifndef UTILITY_H
#define UTILITY_H

#include <utility_interface.h>

namespace othello::utility
{
    /* Query a players bracketing pieces
     * \param[in] board The board
     * \param[in] player_id player's id
     * \param[in] board_pos checking position
     * \return Set of bracketing pieces of a player of a particular position */
    BitPosSet bracketingPieces(const BitBoard& board, const PlayerId& player_id);
    void flipInDir(/*const BitPos& brack_pos,*/ BitBoard& board, const BitPos& board_pos, const PlayerId& player_id, const MoveDirection& dir);
    PlayerId opponent_player(const PlayerId in_player_id);
    void clearBoard(othello::BitBoard& in_board);
    BitPosSet occupiedPosBor(const BitBoard& board, const PlayerId& player_id);
    BitPos bestLegalDepthOneMove(const BitBoard& board, const PlayerId in_player_id);
}   // namespace othello::utility

#endif // UTILITY_H
