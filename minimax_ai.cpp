#include "minimax_ai.h"

// stl
#include <numeric>
#include <iostream>
#include <vector>

namespace othello::minimax_ais
{
    void MiniMaxAI::think(const othello::BitBoard& board,
                          const othello::PlayerId& player_id,
                          const std::chrono::seconds& max_time)
    {
        //m_best_move=othello::utility::bestLegalDepthOneMove(board,player_id);
        //const auto time_start = std::chrono::steady_clock::now();

        size_t level = 2;

        //if (player_id==othello::PlayerId::One) level = 4;


        /* chose the algorithm of thinking */
        //algorithmFirst(board,player_id,level,max_time);
        //algorithmSecond(board,player_id,level,max_time);
        algorithmAlphaBeta(board,player_id,level,max_time);
        return;
    }

    void MiniMaxAI::algorithmFirst(const othello::BitBoard& board,
                        const othello::PlayerId& player_id,
                        const size_t level,
                        const std::chrono::seconds& max_time)
    {
        bool count_costf = true;
        tree.root.child.clear();
        /* create first level of the tree */
        createNodes(tree.root,board,player_id,count_costf);
        /* call createTree function for each child of tree.root but with the opposite player*/
        for (auto& tree_node : tree.root.child ) {
            createTree(tree_node,othello::utility::opponent_player(player_id),level-1,count_costf);
        }
        countMiniMax(tree.root,level);
        m_best_move = findBestMove(tree.root,board);
    }

    void MiniMaxAI::algorithmSecond(const othello::BitBoard& board,
                        const othello::PlayerId& player_id,
                        const size_t level,
                        const std::chrono::seconds& max_time)
    {
        /*
         * creates tree first
         * fills up cost functions for leafs only
         * minmax
        */
        bool count_costf = false;
        cutTree(tree,board);
        createTreeAdv(tree.root,board,player_id,level,count_costf);
        fillCostFuncs(tree.root,player_id);
        countMiniMaxAdv(tree.root,0);
        m_best_move = findBestMove(tree.root,board);
        return;
    }

    void MiniMaxAI::algorithmAlphaBeta(const othello::BitBoard& board,
                        const othello::PlayerId& player_id,
                        const size_t level,
                        const std::chrono::seconds& max_time)
    {

        double alpha = - std::numeric_limits<double>::max();
        double beta =   std::numeric_limits<double>::max();
        tree.root.child.clear();
        tree.root.data.costf=alpha;

        size_t cost = 1;


        //if (MaxPiecesCF) cost=2;
        //else if (MaxTurnCF) cost=3;

        if (player_id==othello::PlayerId::One) cost = 3;

        treeAlphaBeta(tree.root,board,alpha,beta,player_id,level,cost);

        m_best_move = findBestMoveRand(tree.root,board);

        //m_best_move = findBestMove(tree.root,board);

        return;
    }

    BitPos MiniMaxAI::bestMove() const {
        const auto best_move = BitPos(m_best_move.value());
        m_best_move = BitPos::invalid();
        return best_move;
    }

    void MiniMaxAI::createNodes( Node <MiniMaxData>& parent, const othello::BitBoard& board,const othello::PlayerId& player_id, bool fill_costf)
    {
        /* find all legal moves -> constract next boards for them -> input them to noads -> connect them to parent */
        for( const auto& move : othello::utility::legalMoves(board,player_id)) {
                othello::BitBoard temp_board=board;
                /* board after step */
                othello::utility::placeAndFlip(temp_board,player_id,move);
                /* create new child and add to it board */
                Node <MiniMaxData> new_child;
                if (fill_costf) new_child.data.costf=countCostsAdv(temp_board,player_id);
                new_child.data.board=temp_board;
                /* connect child with a parrent */
                parent.child.push_back(new_child);
            }

    }

    void MiniMaxAI::createTree( Node <MiniMaxData>& parent, const othello::PlayerId& player_id,size_t level, bool count_costf)
    {
        if (level==0) { return; }
        createNodes(parent,parent.data.board,player_id, count_costf);
        for (auto& tree_node : parent.child ) {
            createTree(tree_node,othello::utility::opponent_player(player_id),level-1,count_costf);
        }
    }

    void MiniMaxAI::createTreeAdv( Node <MiniMaxData>& parent,const othello::BitBoard& board, const othello::PlayerId& player_id,size_t level, bool count_costf)
    {
        if (level==0) { return; }
        if (parent.child.empty()) createNodes(parent,board,player_id, count_costf);
        for (auto& tree_node : parent.child ) {
            createTreeAdv(tree_node,tree_node.data.board,othello::utility::opponent_player(player_id),level-1,count_costf);
        }
    };

    double MiniMaxAI::countCostsAdv(const othello::BitBoard& board, const othello::PlayerId& player_id)
    {
        /* try to follow the best board places
         * corners best      +100 ( 0, 7,56,63)
         * x-squares worst   -50  ( 9,14,49,54)
         * c-squares worse   -25  ( 1, 6, 8,15,48,55,57,62)
         * x-lines bad       -5   (10,11,12,13,
         *                         22,30,38,46,
         *                         50,51,52,53,
         *                         17,25,33,41)
         * c-neighbor better +25  ( 2, 5,16,23,40,47,58,61)
         * edges      good   +10  ( 3, 4,31,39,59,60,24,32)
         * sweet 16   normal +5   ( 16 others )*/

        double cost_f=0;
        double cur_sweet_price=sweet_price;

        size_t board_s=othello::utility::occupiedPosBor(board,player_id).size()+othello::utility::occupiedPosBor(board,othello::utility::opponent_player(player_id)).size();

        if (board_s<10) cur_sweet_price = 10;

//       if (board_s>53) cost_f = double(-othello::utility::occupiedPosBor(board,player_id).size()+othello::utility::occupiedPosBor(board,othello::utility::opponent_player(player_id)).size());
//        else
//        {
            for (size_t it=0; it<othello::detail::computeBoardSize(); it++)
            {
                if (board[size_t(othello::utility::opponent_player(player_id))].test(it)) {
                    if (binary_search(best_b.begin(),best_b.end(),it)) cost_f+=best_price;
                    else if (binary_search(xsquare_b.begin(),xsquare_b.end(),it)) cost_f+=xsquare_price;
                    else if (binary_search(csquare_b.begin(),csquare_b.end(),it)) cost_f+=csquare_price;
                    else if (binary_search(xlines_b.begin(),xlines_b.end(),it)) cost_f+=xlines_price;
                    else if (binary_search(cnear_b.begin(),cnear_b.end(),it)) cost_f+=cnear_price;
                    else if (binary_search(edges_b.begin(),edges_b.end(),it)) cost_f+=edges_price;
                    else cost_f+=cur_sweet_price;
                }
                else if (board[size_t(player_id)].test(it)) {
                    if (binary_search(best_b.begin(),best_b.end(),it)) cost_f-=best_price;
                    else if (binary_search(xsquare_b.begin(),xsquare_b.end(),it)) cost_f-=xsquare_price;
                    else if (binary_search(csquare_b.begin(),csquare_b.end(),it)) cost_f-=csquare_price;
                    else if (binary_search(xlines_b.begin(),xlines_b.end(),it)) cost_f-=xlines_price;
                    else if (binary_search(cnear_b.begin(),cnear_b.end(),it)) cost_f-=cnear_price;
                    else if (binary_search(edges_b.begin(),edges_b.end(),it)) cost_f-=edges_price;
                    else cost_f-=cur_sweet_price;
                }
            }

//        }

        return cost_f;
    }

    double MiniMaxAI::countCosts(const othello::BitBoard& board, const othello::PlayerId& player_id)
    {
        return double(board[size_t(othello::utility::opponent_player(player_id))].count());
        //return board[size_t(PlayerId(othello::utility::opponent_player(player_id)))].count()-board[size_t(PlayerId(player_id))].count();
    }

    void MiniMaxAI::countMiniMax(Node <MiniMaxData>& parent,size_t level)
    {
        if (level==0) {return;};
        double minCF = std::numeric_limits<double>::max();
        double maxCF = -std::numeric_limits<double>::max();

        for (auto& tree_node : parent.child ) {
            countMiniMax(tree_node,level-1);
            if (level%2!=0) {
                    minCF=std::min(tree_node.data.costf,minCF);
                    parent.data.costf=minCF;
            }
            else {
                    maxCF=std::max(tree_node.data.costf,maxCF);
                    parent.data.costf=maxCF;
            };
        }
        return;
    }

    void MiniMaxAI::fillCostFuncs(Node <MiniMaxData>& parent, const othello::PlayerId& player_id)
    {
        for (auto& tree_node : parent.child ) {
            if (tree_node.child.empty()) {
                tree_node.data.costf=countCostsAdv(tree_node.data.board,player_id);
            }
            fillCostFuncs(tree_node,othello::utility::opponent_player(player_id));
        }
        return;
    }

    BitPos MiniMaxAI::findBestMove(Node <MiniMaxData>& parent,const othello::BitBoard& board)
    {
        BitBoard bestBoard;
//        double max = -std::numeric_limits<double>::max();;
        for (auto& tree_node : parent.child ) {
//            if (tree_node.data.costf>max)
            if (tree_node.data.costf>=parent.data.costf)
            {
//                max=std::max(tree_node.data.costf,max);
                bestBoard=tree_node.data.board;
            }
        }
        return findTurn(board,bestBoard);
    }

    BitPos MiniMaxAI::findBestMoveRand(Node <MiniMaxData>& parent,const othello::BitBoard& board)
    {
        std::vector<BitBoard> bestBoards;
        std::random_device rd; // obtain a random number from hardware
        std::mt19937 eng(rd()); // seed the generator

        for (auto& tree_node : parent.child ) {
            if (tree_node.data.costf>=parent.data.costf)
            {
                bestBoards.push_back(tree_node.data.board);
            }
        }
        std::uniform_int_distribution<> distr(0, int(bestBoards.size()-1)); // define the range
        return findTurn(board,bestBoards[size_t(distr(eng))]);
    }

    BitPos MiniMaxAI::findTurn(const othello::BitBoard& board1, const othello::BitBoard& board2)
    {
        BitPieces occupied1=othello::utility::occupiedPositions(board1);
        BitPieces occupied2=othello::utility::occupiedPositions(board2);

        for (size_t i=0;i<detail::computeBoardSize();i++) {
            if (occupied1.test(i) and !occupied2.test(i)) { return BitPos(i);}
            else if (!occupied1.test(i) and occupied2.test(i)) { return BitPos(i);}
        }
        return BitPos().invalid();
    }

    MiniMaxAI::MiniMaxAI(const PlayerId&) {
//      m_engine.seed(m_rd());

    }

    void MiniMaxAI::cutTree( MinMaxTree& in_tree,const othello::BitBoard& board)
    {
        findCutNode(in_tree.root,board);
        return;
    };

    void MiniMaxAI::findCutNode(Node <MiniMaxData>& parent, const othello::BitBoard& board)
    {
        for (auto tree_node : parent.child ) {
            if (tree_node.data.board==board) {
                tree.root= tree_node;
                return;
            }
            findCutNode(tree_node,board);
        }
    };

    void MiniMaxAI::countMiniMaxAdv(Node <MiniMaxData>& parent,size_t level)
    {
        if (!parent.child.empty()) {
            if (level%2==0) parent.data.costf = -std::numeric_limits<double>::max();
            else parent.data.costf = std::numeric_limits<double>::max();
        }
        for (auto& tree_node : parent.child) {
            countMiniMaxAdv(tree_node,level+1);
            if (level%2==0) parent.data.costf=std::max(tree_node.data.costf,parent.data.costf);
            else parent.data.costf=std::min(tree_node.data.costf,parent.data.costf);
        }
        return;
    };

    void MiniMaxAI::treeAlphaBeta( Node <MiniMaxData>& parent, const othello::BitBoard& board, double alpha, double beta, const othello::PlayerId& player_id,size_t level, size_t cost)
    {
        if (level==0) { return; }
        for( const auto& move : othello::utility::legalMoves(board,player_id)) {
            othello::BitBoard temp_board=board;
            othello::utility::placeAndFlip(temp_board,player_id,move);
            Node <MiniMaxData> new_child;
            if ((level!=1) and (!othello::utility::legalMoves(temp_board,othello::utility::opponent_player(player_id)).empty())) {
                if (level%2==0)  new_child.data.costf= std::numeric_limits<double>::max();
                else new_child.data.costf= -std::numeric_limits<double>::max();
            } else
                if (cost_turn) new_child.data.costf = countCostsStep(temp_board,board,player_id);
                else new_child.data.costf = countCostsAdv(temp_board,player_id);

            if (   ((new_child.data.costf<parent.data.costf) and (level%2!=0))
                or ((new_child.data.costf>parent.data.costf) and (level%2==0))) {

                new_child.data.board=temp_board;
                parent.child.push_back(new_child);
                treeAlphaBeta(parent.child.back(),parent.child.back().data.board,alpha,beta,othello::utility::opponent_player(player_id),level-1, cost);
                if (level%2==0) {
                    parent.data.costf=std::max(parent.child.back().data.costf,parent.data.costf);
                    alpha=std::max(parent.data.costf,alpha);
                    if (beta<alpha) return;
                }
                else {
                    parent.data.costf=std::min(parent.child.back().data.costf,parent.data.costf);
                    beta=std::min(parent.data.costf,beta);
                    if (beta<alpha) return;
                };

            }

        };

    };

    double MiniMaxAI::countCostsStep(const othello::BitBoard& board,const othello::BitBoard& board_before, const othello::PlayerId& player_id)
    {
        //return double(board[size_t(player_id)].count()-board_before[size_t(player_id)].count());
        return double(board[size_t(othello::utility::opponent_player(player_id))].count()-board_before[size_t(othello::utility::opponent_player(player_id))].count());
    }


    MiniMaxPieces::MiniMaxPieces(const PlayerId& player_id) : MiniMaxAI(player_id) {

    }

    double MiniMaxPieces::countCostsAdv(const othello::BitBoard& board, const othello::PlayerId& player_id){
        return double(board[size_t(othello::utility::opponent_player(player_id))].count());
    }

    MiniMaxTurn::MiniMaxTurn(const PlayerId& player_id) : MiniMaxAI(player_id) {
        cost_turn=true;
    }

}
