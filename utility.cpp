#include "utility.h"

// stl
#include "numeric"
#include "iostream"


namespace othello::utility
{
  ////////////////////
  //
  //
  // Interface Utility
  // Functions
  //
  //
  ////////////////////

  BitPieces occupiedPositions(const BitBoard& board)
  {

    return board[size_t(othello::PlayerId::One)]|board[size_t(othello::PlayerId::Two)];
  }

  BitPosSet occupiedPosBor(const BitBoard& board, const PlayerId& player_id)
  {
      BitPosSet return_set;
      for (size_t i=0; i<64; i++) {
          if (board[size_t(player_id)][i]) {
              return_set.insert(BitPos(i));
          };
      }
      return return_set;
  }

  bool occupied(const BitPieces& pieces, const BitPos& board_pos)
  {
      return pieces.test(board_pos.value());
  }

  bool occupied(const BitBoard& board, const BitPos& board_pos)
  {
    return occupiedPositions(board).test(board_pos.value());
  }

  BitPos nextPosition(const BitPos& board_pos, const MoveDirection& dir)
  {
    /* return next position in the direction or the same position, if next position is out of board */
    switch (dir) {
        case MoveDirection::N : {
            /* current position +8
             * except top line
             * check if current position+8 is in board
            */
            //if (BitPos(board_pos.value()+8).isValid()) {return BitPos(board_pos.value()+8);}
            if ((board_pos.value()+8)<detail::invalidBoardPosition()-1) {return BitPos(board_pos.value()+8);}
            else { return board_pos.invalid();}
            //else { return board_pos;}
            };
        case MoveDirection::W : {
            /* current position +1
             * except most left column
             * check (current position + 1) not divides by 8
             * if ((BitPos(board_pos.value()+1).isValid()) and (((board_pos.value()+1)%8)!=0))
            */
            if (((board_pos.value()+1)%8)!=0) {return BitPos(board_pos.value()+1);}
            else { return board_pos.invalid();}
            //else { return board_pos;}
            };
        case MoveDirection::S : {
            /* current position -8
             * except bottom line
             * check if current position-8 is in board
            if (BitPos(board_pos.value()-8).isValid()) {return BitPos(board_pos.value()-8);}
            else { return board_pos;};
            */
            if (board_pos.value()>7) {return BitPos(board_pos.value()-8);}
            else { return board_pos.invalid();}
            //else { return board_pos;}
            };
        case MoveDirection::E : {
            /* current position -1
             * except most right column
             * check (current position) not divides by 8
            */
            if ((board_pos.value()%8)!=0) {return BitPos(board_pos.value()-1);}
            else { return board_pos.invalid();}
            //else { return board_pos;}
            };
        case MoveDirection::NE : {
            /* current position +7
             * except most right column and top line
             * check N and E
            */
            if (((board_pos.value()+8)<detail::invalidBoardPosition()-1) and ((BitPos(board_pos.value()-1).isValid()) and ((board_pos.value()%8)!=0))) {return BitPos(board_pos.value()+7);}
            else { return board_pos.invalid();}
            //else { return board_pos;}
            };
        case MoveDirection::NW : {
            /* current position +9
             * except most left column and top line
             * check N and W
            */
            if (((board_pos.value()+8)<detail::invalidBoardPosition()-1) and (((board_pos.value()+1)%8)!=0)) {return BitPos(board_pos.value()+9);}
            else { return board_pos.invalid();}
            //else { return board_pos;}
            };
        case MoveDirection::SW : {
            /* current position -7
             * except most left column and bottom line
             * check S and W
            */
            if ((board_pos.value()>7) and (((board_pos.value()+1)%8)!=0)) {return BitPos(board_pos.value()-7);}
            else { return board_pos.invalid();}
            //else { return board_pos;}
            };
        case MoveDirection::SE : {
            /* current position -9
             * except most right column and bottom line
             * check S and E
            */
            if ((board_pos.value()>7) and ((board_pos.value()%8)!=0)) {return BitPos(board_pos.value()-9);}
            else { return board_pos.invalid();}
            //else { return board_pos;}
            };
    }
  }

  /* not tested - test it*/

  BitPos findBracketingPiece(const BitBoard& board,
                             const BitPos& board_pos,
                             const PlayerId& player_id,
                             const MoveDirection& dir)
  {
      BitPos temp_board_pos = board_pos;
      bool first_iter = true;

      if (!occupied(board.at(size_t(player_id)),temp_board_pos)) {return BitPos().invalid();};

      do {
        /* to exclude exceptions of out of board situations or when input position is not occupied by input players piece */
        if (othello::utility::nextPosition(temp_board_pos,dir)==BitPos().invalid()) {return BitPos().invalid();};
        /* if next pos is out of board or empty and it is a first turn, then there is no BracketingPiece return invalid bitpos */
        if (first_iter and !(othello::utility::occupied(board,(othello::utility::nextPosition(temp_board_pos,dir))))) { return BitPos().invalid(); }
        /* if next pos is occupied by yours piece, then there is no BracketingPiece return invalid bitpos */
        else if ((occupied(board.at(size_t(player_id)),(othello::utility::nextPosition(temp_board_pos,dir))))) { return BitPos().invalid(); }
        /* if next pos is empty and it is NOT a first turn, then it is a BracketingPiece return that bitpos
         * it could also be out of bord but we still return bitpos, hopefully it will be the same as BitPos.invalid()
        */
        else if (!(first_iter) and !(othello::utility::occupied(board,(othello::utility::nextPosition(temp_board_pos,dir))))) { return othello::utility::nextPosition(temp_board_pos,dir); }
        /* if next pos is occupied by the other player, then if you continue to move in the same way it could be BracketingPiece so continue moving */
        else if ((othello::utility::occupied(board,(othello::utility::nextPosition(temp_board_pos,dir)))) and !(occupied(board.at(size_t(player_id)),(othello::utility::nextPosition(temp_board_pos,dir)))))
        {
            if (first_iter) {first_iter=false;};
            temp_board_pos = othello::utility::nextPosition(temp_board_pos,dir);
        }
        /* if nothing worked just to return something */
        else return BitPos().invalid();;
      } while (!first_iter);

  }

  /* return set of bracketing pieces of inputed piece */
  BitPosSet bracketingPieces(const BitBoard& board, const PlayerId& player_id)
  {
      BitPos tempBrack;
      BitPosSet returnSet;

      /* check all the pieces of input player */

      for (size_t i=0;i<board[size_t(player_id)].size();i++)
      {
          /* check all the dirrections */

          tempBrack=othello::utility::findBracketingPiece(board,BitPos(i),player_id,othello::MoveDirection::N);
          if (tempBrack!=BitPos().invalid()) {returnSet.insert(tempBrack);};
          tempBrack=othello::utility::findBracketingPiece(board,BitPos(i),player_id,othello::MoveDirection::S);
          if (tempBrack!=BitPos().invalid()) {returnSet.insert(tempBrack);};
          tempBrack=othello::utility::findBracketingPiece(board,BitPos(i),player_id,othello::MoveDirection::E);
          if (tempBrack!=BitPos().invalid()) {returnSet.insert(tempBrack);};
          tempBrack=othello::utility::findBracketingPiece(board,BitPos(i),player_id,othello::MoveDirection::W);
          if (tempBrack!=BitPos().invalid()) {returnSet.insert(tempBrack);};
          tempBrack=othello::utility::findBracketingPiece(board,BitPos(i),player_id,othello::MoveDirection::NE);
          if (tempBrack!=BitPos().invalid()) {returnSet.insert(tempBrack);};
          tempBrack=othello::utility::findBracketingPiece(board,BitPos(i),player_id,othello::MoveDirection::NW);
          if (tempBrack!=BitPos().invalid()) {returnSet.insert(tempBrack);};
          tempBrack=othello::utility::findBracketingPiece(board,BitPos(i),player_id,othello::MoveDirection::SE);
          if (tempBrack!=BitPos().invalid()) {returnSet.insert(tempBrack);};
          tempBrack=othello::utility::findBracketingPiece(board,BitPos(i),player_id,othello::MoveDirection::SW);
          if (tempBrack!=BitPos().invalid()) {returnSet.insert(tempBrack);};
      }
      return returnSet;
  }

  BitPosSet legalMoves(const BitBoard& board, const PlayerId& player_id)
  {
      BitPos tempBrack;
      BitPosSet returnSet;

      /* check all the pieces of input player */
      for (size_t i=0;i<board[size_t(player_id)].size();i++)
      {
          /* check all the dirrections */
          tempBrack=othello::utility::findBracketingPiece(board,BitPos(i),player_id,othello::MoveDirection::N);
          if (tempBrack!=BitPos().invalid()) {returnSet.insert(tempBrack);};
          tempBrack=othello::utility::findBracketingPiece(board,BitPos(i),player_id,othello::MoveDirection::S);
          if (tempBrack!=BitPos().invalid()) {returnSet.insert(tempBrack);};
          tempBrack=othello::utility::findBracketingPiece(board,BitPos(i),player_id,othello::MoveDirection::E);
          if (tempBrack!=BitPos().invalid()) {returnSet.insert(tempBrack);};
          tempBrack=othello::utility::findBracketingPiece(board,BitPos(i),player_id,othello::MoveDirection::W);
          if (tempBrack!=BitPos().invalid()) {returnSet.insert(tempBrack);};
          tempBrack=othello::utility::findBracketingPiece(board,BitPos(i),player_id,othello::MoveDirection::NE);
          if (tempBrack!=BitPos().invalid()) {returnSet.insert(tempBrack);};
          tempBrack=othello::utility::findBracketingPiece(board,BitPos(i),player_id,othello::MoveDirection::NW);
          if (tempBrack!=BitPos().invalid()) {returnSet.insert(tempBrack);};
          tempBrack=othello::utility::findBracketingPiece(board,BitPos(i),player_id,othello::MoveDirection::SE);
          if (tempBrack!=BitPos().invalid()) {returnSet.insert(tempBrack);};
          tempBrack=othello::utility::findBracketingPiece(board,BitPos(i),player_id,othello::MoveDirection::SW);
          if (tempBrack!=BitPos().invalid()) {returnSet.insert(tempBrack);};
      }
      return returnSet;
    return {};
  }

  bool isLegalMove(const BitBoard& board, const PlayerId& player_id,
                   const BitPos& board_pos)
  {
    bool return_value=false;
    BitPosSet legalMoves = othello::utility::legalMoves(board, player_id);
    if (legalMoves.size()>0) {
        for (auto i=legalMoves.begin(); i != legalMoves.end() ; ++i) {
            if ( board_pos == *i) {return_value=true;};
        }
    }
    return return_value;
  }

  void flipInDir(/*const BitPos& brack_pos,*/ BitBoard& board, const BitPos& board_pos, const PlayerId& player_id, const MoveDirection& dir)
  {
      BitPos temp_pos = board_pos;
      PlayerId opp_id;
      BitPosSet flip_set;
      bool reachBrack=false;
      bool exitDo=false;

      opp_id = opponent_player(player_id);
      /* if there is a brackpoint then flip all the opponents pieces in its dirrection*/
      do {
            /* if next pos is occupied by your piece, then you've reache bracket, so break */
            temp_pos = nextPosition(temp_pos,dir);
            if (temp_pos==BitPos().invalid()) {return;};
            if (occupied(board[size_t(player_id)],temp_pos)) { reachBrack=true; break;}
            else  if (!occupied(board,temp_pos)) { exitDo=true;}
            else
            {
                /* make list of possible flips*/
                flip_set.insert(temp_pos);
            }
          } while (!exitDo);
          /* if we reach piece of our color, then flip collected flips*/
          if ((flip_set.size()>0) and (reachBrack)) {

//            score_player_o=score_player_o+int(flip_set.size());

            for (auto i=flip_set.begin(); i != flip_set.end() ; ++i) {
                temp_pos = *i;
                board[size_t(opp_id)].flip(temp_pos.value());
                board[size_t(player_id)].flip(temp_pos.value());
            }
          }
      return;
  }

  void placeAndFlip(BitBoard& board, const PlayerId& player_id,
                    const BitPos& board_pos)
  {
      if (occupied(board,board_pos)) {return;} else
      if (!isLegalMove(board,player_id,board_pos)) {return;} else
      {
          /* flip input piece*/
          board[size_t(player_id)].flip(board_pos.value());
          BitPosSet returnSet;
          MoveDirection dir;
          /* check all the dirrections */
          dir = othello::MoveDirection::N;
          flipInDir(board, board_pos, player_id,dir);
          dir = othello::MoveDirection::S;
          flipInDir(board, board_pos, player_id,dir);
          dir = othello::MoveDirection::E;
          flipInDir(board, board_pos, player_id,dir);
          dir = othello::MoveDirection::W;
          flipInDir(board, board_pos, player_id,dir);
          dir = othello::MoveDirection::NE;
          flipInDir(board, board_pos, player_id,dir);
          dir = othello::MoveDirection::NW;
          flipInDir(board, board_pos, player_id,dir);
          dir = othello::MoveDirection::SE;
          flipInDir(board, board_pos, player_id,dir);
          dir = othello::MoveDirection::SW;
          flipInDir(board, board_pos, player_id,dir);
          return ;
      };

  }

  PlayerId opponent_player(const PlayerId in_player_id)
  {
      if (in_player_id==othello::PlayerId::One) { return othello::PlayerId::Two;}
      else return othello::PlayerId::One;
  }

  void clearBoard(othello::BitBoard& in_board){
      in_board[size_t(othello::PlayerId::One)].reset();
      in_board[size_t(othello::PlayerId::Two)].reset();
  }

  BitPos bestLegalDepthOneMove(const BitBoard& board, const PlayerId in_player_id){
      for (auto first_legal : othello::utility::legalMoves(board,in_player_id)) {
        return first_legal;
      }
      return BitPos().invalid();
  }

}   // namespace othello::utility
