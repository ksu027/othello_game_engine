#ifndef MINIMAX_AI_H
#define MINIMAX_AI_H

#include <player_interface.h>
#include <random>
#include <engine.h>
#include <utility.h>
#include "utility_interface.h"

namespace othello::minimax_ais
{

  class MiniMaxAI : public AIInterface {

  private:
      mutable BitPos m_best_move,m_best_move2;
      std::random_device m_rd;
      std::mt19937       m_engine;

      std::vector<size_t> const best_b    = { 0, 7,56,63};
      std::vector<size_t> const xsquare_b = { 9,14,49,54};
      std::vector<size_t> const csquare_b = { 1, 6, 8,15,
                                             48,55,57,62};
      std::vector<size_t> const xlines_b  = {10,11,12,13,
                                             17,22,25,30,
                                             33,38,41,46,
                                             50,51,52,53};
      std::vector<size_t> const cnear_b   = { 2, 5,16,23,40,47,58,61};
      std::vector<size_t> const edges_b   = { 3, 4,24,31,32,39,59,60};
      double const best_price    = 100.00;
      double const cnear_price   =  25.00;
      double const edges_price   =  10.00;
      double sweet_price   =   5.00;
      double const xlines_price  =  -5.00;
      double const csquare_price = -45.00;
      double const xsquare_price = -70.00;

  public:

      struct MiniMaxData {
        BitBoard board;
        double costf;
      };

      template <typename T> struct Node {
          std::vector <Node <T>> child;
          T data;
      };

      struct MinMaxTree {
          Node <MiniMaxData> root;
      };

      MinMaxTree tree,tree2;

      bool cost_turn=false;


      void   think(const BitBoard& board, const PlayerId& player_id,
                   const std::chrono::seconds& max_time) override;
      void algorithmFirst(const othello::BitBoard& board,
                          const othello::PlayerId& player_id,
                          const size_t level,
                          const std::chrono::seconds& max_time);
      void algorithmSecond(const othello::BitBoard& board,
                          const othello::PlayerId& player_id,
                          const size_t level,
                          const std::chrono::seconds& max_time);
      void algorithmAlphaBeta(const othello::BitBoard& board,
                          const othello::PlayerId& player_id,
                          const size_t level,
                          const std::chrono::seconds& max_time);
      void createNodes( Node <MiniMaxData>& parent, const othello::BitBoard& board,const othello::PlayerId& player_id, bool fill_costf);
      void createTree( Node <MiniMaxData>& parent, const othello::PlayerId& player_id,size_t level, bool count_costf);
      void fillCostFuncs(Node <MiniMaxData>& parent, const othello::PlayerId& player_id);
      double countCosts(const othello::BitBoard& board, const othello::PlayerId& player_id);
      virtual double countCostsAdv(const othello::BitBoard& board, const othello::PlayerId& player_id);
      double countCostsStep(const othello::BitBoard& board,const othello::BitBoard& board_before, const othello::PlayerId& player_id);
      BitPos bestMove() const override;
      void  countMiniMax(Node <MiniMaxData>& parent,size_t level);
      void  countMiniMaxAdv(Node <MiniMaxData>& parent,size_t level);
      BitPos findBestMove(Node <MiniMaxData>& parent,const othello::BitBoard& board);
      BitPos findBestMoveRand(Node <MiniMaxData>& parent,const othello::BitBoard& board);
      BitPos findTurn(const othello::BitBoard& board1, const othello::BitBoard& board2);
      MiniMaxAI(const PlayerId& player_id);
      void cutTree( MinMaxTree& in_tree,const othello::BitBoard& board);
      void findCutNode(Node <MiniMaxData>& parent, const othello::BitBoard& board);
      void createTreeAdv( Node <MiniMaxData>& parent,const othello::BitBoard& board, const othello::PlayerId& player_id,size_t level, bool count_costf);
      void treeAlphaBeta( Node <MiniMaxData>& parent, const othello::BitBoard& board, double alpha, double beta, const othello::PlayerId& player_id,size_t level, size_t cost);
  }; 

  class MiniMaxPieces : public MiniMaxAI  {
    public:
      MiniMaxPieces(const PlayerId& player_id);
      double countCostsAdv(const othello::BitBoard& board, const othello::PlayerId& player_id) override;
  };

  class MiniMaxTurn : public MiniMaxAI  {
    public:
      MiniMaxTurn(const PlayerId& player_id);
  };
}   // namespace othello::minimax_ais

#endif // MINIMAX_AI_H
